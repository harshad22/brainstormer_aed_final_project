# Brainstormer_AED_Final_Project

Project Summary: To create a prototype of a global vaccination solution with an aim to provide vaccinations and health service to the country with high child mortality rates.

Key Functionalities:
1. Creating a system to provide and maintain data of vaccinations available in a country with an interconnected network.
2. As per the trend of increasing mortality rates, creating a platform for physicians to provide medical training to chosen physicians in a country.
3. Organizing events in orphanges for providing them with care and vaccination services.

Special Features
1. Graphs and reporting systems with the analysed data for vaccination services.
2. Email system integrated to generate email to the country, admin to initiate a request depending upon the vaccination availability as per the recent mortality rates
3. Track of physician visits across countries to educate and boost the medical facilities.